﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Rigidbody))]
public class PlayerController : MonoBehaviour
{  //for player controll

    Vector3 velocity;
    Rigidbody myRegidbody; //Todo. 2d ridbody로 변경.

    // Use this for initialization
    void Start () {
        myRegidbody = GetComponent<Rigidbody>();
	}
	
    public void Move(Vector3 moveVelocity)
    {
        velocity = moveVelocity;
        
    }

    public void LookAt(Vector3 lookPoint) //플레이어가 커서가 보는 방향으로 회전. 카메라로부터 커서의 위치를 통과하도록 ray를 쏘아서 바닥에 도착하는 지점을 플레이어가 바라보도록.
    {
        Vector3 heightCorrectedPoint = new Vector3(lookPoint.x, transform.position.y, lookPoint.z); //y값은 플레이어 자신의 높이를줌. (기울여지지 않게 하기 위해서)
       // transform.LookAt(point); //해당방향으로 기울여짐 
        transform.LookAt(heightCorrectedPoint); 
    }

    void FixedUpdate() //정기적으로 반복됨. 그래야 다른 오브젝트랑 안겹친다?
                       //프레임저하 발생시에도 프레임에 시간 가중치를 곱해 실행되어 이동속도가 유지된다
    {
        myRegidbody.MovePosition(myRegidbody.position + velocity * Time.fixedDeltaTime);
    }

    //https://youtu.be/jdv8erC7ML8?t=508
}
