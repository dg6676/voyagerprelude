﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour //일정 주기마다 웨이브에 따라 적이 몰려오는 형태.
                                    //1웨이브 동안 적은 한마리씩 웨이브 내에 정의한 숫자만큼 스폰된다.
{
    public Wave[] waves;
    public Enemy enemy;

    Wave currentWave; //현재 웨이브의 레퍼런스

    int enemiesRemainingToSpawn; //남아있는 스폰할 적.
    int enemiesRemaningAlive;    // 살아있는 적의 수
    int currentWaveNumber;      //현재 웨이브의 횟수를 나타낼 정수
    float nextSpawnTime;        // 다음번 스폰시간

    private void Start()
    {
        NextWave();
    }

    void Update()
    {
        if (enemiesRemainingToSpawn > 0 && Time.time > nextSpawnTime) { //적을 스폰.
            enemiesRemainingToSpawn--;
            nextSpawnTime = Time.time + currentWave.timeBetweenSpawns;

            Enemy spawnedEnemy = Instantiate(enemy, Vector3.zero, Quaternion.identity) as Enemy; //enemy 프리팹을 오브젝트화.
            spawnedEnemy.OnDeath += OnEnemyDeath; // 스폰되는 적에게 이벤트에 함수를 등록해줌.
        }
    }

    void OnEnemyDeath() { //적이 죽을때마다 적이 이벤트(등록된 이 함수)를 호출하면서 이쪽으로 알림을 받게됨
        print("enemy died");
        enemiesRemaningAlive--;

        if (enemiesRemaningAlive == 0) {
            NextWave();
        }
    }

    void NextWave() { //다음번 웨이브 생성
        currentWaveNumber++;
        print("wave:" + currentWaveNumber);
        if (currentWaveNumber - 1 < waves.Length)
        {
            currentWave = waves[currentWaveNumber - 1];
            enemiesRemainingToSpawn = currentWave.enemyCount;
            enemiesRemaningAlive = enemiesRemainingToSpawn;
        }
    }

    [System.Serializable] //유니티 인스펙터에 보여줌.
    public class Wave {
        public int enemyCount;
        public float timeBetweenSpawns; //스폰 간격

    }   
}
