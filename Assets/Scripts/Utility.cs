﻿using UnityEngine;

public static class Utility
{
    public static T[] ShuffleArray<T> (T[]array, int seed) //셔플된 배열반환. 랜덤시드값
    {
        System.Random prng = new System.Random(seed);
        for (int i=0;i<array.Length-1; i++) { //the fisher-yates. 마지막 루프는 생략하는 알고리즘임.
            int randomIndex = prng.Next(i, array.Length); //최소값, 최대값. 
            T tempItem = array[randomIndex];       //랜덤번째 원소 저장
            array[randomIndex] = array[i];         //i번째 원소를 랜덤번째에 할당
            array[i] = tempItem;                   //i번째 원소 자리에 랜덤번째 할당
            //i자리와 랜덤자리 원소 교환하는것.
        }

        return array;
    }
}
