﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : LivingEntity
{
    public enum State {Idle, Chasing, Attacking }
    State currentState;

    NavMeshAgent pathfinder;
    Transform target;
    LivingEntity targetEntity; //플레이어의 체력 등
    Material skinMaterial;
    Color orinalColor;

    float attackDistanceThreshold=0.5f; //공격거리임계값. 공격할수있는 한계거리. 유니티 단위1=1meter
    float timeBetweenAttacks = 1;       //r공격사이간격
    float damage = 1;
    float nextAttackTime;               //다음 공격 가능시간
    float myCollisionRadius;
    float targetCollisionRadius;

    bool hasTarget; //플레이어가 살아있는지 확인용도

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        pathfinder = GetComponent<NavMeshAgent>();
        skinMaterial = GetComponent<Renderer>().material;
        orinalColor = skinMaterial.color;

        if(GameObject.FindGameObjectWithTag("Player") != null) //적이 스폰됐을때 플레이어가 이미 죽엇는지.
        {
            hasTarget = true;
            currentState = State.Chasing; //디폴트 상태.
            target = GameObject.FindGameObjectWithTag("Player").transform;

            targetEntity = target.GetComponent<LivingEntity>();
            targetEntity.OnDeath += OnTargetDeath;

            myCollisionRadius = GetComponent<CapsuleCollider>().radius;
            targetCollisionRadius = target.GetComponent<CapsuleCollider>().radius;

            StartCoroutine(UpdatePath());
        }
       
    }

    void OnTargetDeath()
    {
        hasTarget = false;
        currentState = State.Idle;
    }

    // Update is called once per frame
    void Update(){
        //pathfinder.SetDestination(target.position); //매프레임마다 새롭게 경로를 계산해서 안좋.
        //매프레임마다 업데이트하지않고 타이머로 고정된 간격으로 업데이트 시킬것.
        if (hasTarget){
            if (Time.time > nextAttackTime)
            { //공격가능한 시간인지 확인
              //Vector3.Distance();//제곱근 연산이라서 비쌈. 실제값보다는 그저 값을 비교할때는 제곱형태만 가지고비교(제곱근연산x) 
                float sqrDstToTarget = (target.position - transform.position).sqrMagnitude; //차를 제곱한값

                if (sqrDstToTarget < Mathf.Pow(attackDistanceThreshold + myCollisionRadius + targetCollisionRadius, 2)) // 대상과의 거리를 중심이 아닌 콜리더 표면부터 계산하기 위해 반지름더함.
                {//공격거리를 2만큼 제곱한다는 의미
                    nextAttackTime = Time.time + timeBetweenAttacks;
                    StartCoroutine(Attack()); //잠시 주석
                }
            }
        }
        
    }

    IEnumerator Attack() { //적이 찌르는 공격 (찌를 위치로 이동했다가 다시 원래위치로 돌아오는 공격)
        currentState = State.Attacking;
        pathfinder.enabled = false; //공격 중에는 추적하며 이동 x 이상태에서 별다른 처리없이 SetDestination 호출하면 오류가 발생할 가능성있음.

        Vector3 originalPosition = transform.position;
        Vector3 dirToTarget = (target.position - transform.position).normalized; //방향벡터
        Vector3 attackPosition = target.position - dirToTarget * (myCollisionRadius); // + targetCollisionRadius ); 총돌이 조금 안쪽까지 부딪히도록 거리를 줄여봄.
        //Vector3 attackPosition = target.position;
        //공격 타겟 포인트를 타겟 중앙이 아니라 콜리더 표면 위치로 변경한것.

        float attackSpeed = 3;   //공격속도. (공격애니메이션 속도)
        float percent = 0;       //0~1, 어느 거리만큼 어택을 할것인지. 
        skinMaterial.color = Color.red;

        bool hasAppliedDamage = false; //데미지를 적용하는 도중인지

        //보간값(interpolation) 원지점->공격지점으로 이동시 그래프의 대칭곡선을 만드는 '참조점'임. 
        //y = 4(-x^2+x) -> 0부터 1까지 대칭적인 볼록 2차함. 그래프의 x값은 percent. y값은 interpolation에 해당.

        //공격의 한 사이클. percent 0~1
        while (percent <= 1) {   //originalPosition -> attackPosition -> originalPosition  : interpolation 0 -> 1 -> 0 이렇게 하기 위해 위의 대칭함수사용.
            if (percent >= .5f && !hasAppliedDamage)  //percent가 0.5일떄 interpolation 이 1임(= attackPosition 도달.). 
            {
                hasAppliedDamage = true;
                targetEntity.TakeDamage(damage);
            }

            percent += Time.deltaTime * attackSpeed;
            float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
            transform.position = Vector3.Lerp(originalPosition, attackPosition, interpolation); // 두 벡터사이의 비례값(0에서1)으로 내분점 지점반환.

            yield return null; 
            // while 루프의 각 처리 사이에서 프레임을 스킵. 
            // 이지점에서 작업이 멈추고 update 메소드의 작업이 한번 수행된후 다음프레임으로 넘어갈때, yield 다음 코드나, 다음 루프가 실행됨.
        }

        skinMaterial.color = orinalColor;
        currentState = State.Chasing;
        pathfinder.enabled = true; //공격 종료 후
    }

    IEnumerator UpdatePath()
    {
        float refreshRate = .25f;//1초에 4번 경로갱신
       
        while (hasTarget){ //refreshDate 마다 루프 반복
            if (currentState == State.Chasing) {
                //적과 목표 사이의 방향(방향벡터=정규화(대상위치-현재위치))에 적과 목표의 충돌범위 반지름을 곱하여 뺀값을 할당.
                Vector3 dirToTarget = (target.position - transform.position).normalized; //방향벡터
                Vector3 targetPosition = target.position - dirToTarget * (myCollisionRadius + targetCollisionRadius + attackDistanceThreshold/2); 
                // 콜리더 경계면이 아닌attackDistanceThreshold/2 만큼 띄어서 멈추게함. 
                //new Vector3(target.position.x, 0, target.position.z);
                if (!dead)
                { //적이 파괴된 이후에도 코루틴이 실행되면서 아래 코드를 호출할 경우 오류가 날수있으므로 방지.
                    pathfinder.SetDestination(targetPosition);
                }
            }
            yield return new WaitForSeconds(refreshRate);
        }
        //yield return을 if문 안쪽에 두니까 게임이 멈춰버림 왜??
    }
}
