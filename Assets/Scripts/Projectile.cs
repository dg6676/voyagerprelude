﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour //발사체
{
    public LayerMask collisionMask; //어떤 오브젝트, 레이어가 발사체 와 충돌하는지 결정. Enemy Layer를 추가하고. Enemy 오브젝트의 레이어를 Enemy로 설정한뒤, 여기에 레이어할당.
    float speed = 10;
    float damage = 1;
    float lifeTime = 3; //총알의 생존시간
    float skinWidth = .1f;

    //총알이 적에 근접한다음에 적의 내부위치에서 생성되서 발사될경우 충돌판정없이 그냥지나감(RayCast의 특성). 
    //이를 해결하기위해 어떤 콜라이더 내부에서 출발하는지 체크필요

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, lifeTime); //소문자 gameObject는 자신. lifeTime 경과후파괴

        Collider[] initialCollisions = Physics.OverlapSphere(transform.position, .1f, collisionMask); //위치, 반지름, 레이어마스크
        //현재 발사체와 겹쳐져있는 모든 충돌체의 배열.
        if(initialCollisions.Length > 0) //발사체 생성시 어떤 콜라이더와 이미 겹칠상태일때.
        {
            OnHitObject(initialCollisions[0]);
        }
    }

    public void SetSpeed(float newSpeed) {
        speed = newSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        //레이의 이동할 거리와 총돌에 대한 결과 계산.
        float moveDistance = speed * Time.deltaTime;  //현재 프레임에서 충돌 거리 계산 ???
        CheckCollisions(moveDistance);
        transform.Translate(Vector3.forward * moveDistance);
    }

    void CheckCollisions(float moveDistance) //따로함수를 만든 이유: update메소드를 간결하게 만들고 여러 메소들을 내부에서 조합만 하는 지휘소 스타일이 좋다.
    {
        Ray ray = new Ray(transform.position, transform.forward); //발사체위치, 발사체정면방향
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, moveDistance+skinWidth, collisionMask, QueryTriggerInteraction.Collide)) //ray를 쏘고 hit를 가져옴(out) QueryTriggerInteraction 트리거 콜라이더들과 충돌 여부 결정.
        {                                                                                                //콜라이더의 isTrigger 옵션을 켜면 캐릭터들끼리 밀리지않고 겹쳐짐.
            OnHitObject(hit);                                                                            //레이캐스트가 QueryTriggerInteraction 쓰면 트리거 콜라이더와의 충돌도 가져올수있음
                                                                                                         //ray가 이동할 거리에 skinWidth(보정값?)을 더하는 이유는 한프레임안에서 적이 빠르게 이동해올 경우 적과 겹쳐진 상태에서
                                                                                                         //rayCast하게되면 충돌감지가 되지 않을수있기때문에 한프레임에서 발사체가 이동할거리를 조금더 늘려줌 
                                                                                                         //위 방식은 콜라더끼리 실제 겹치기전에 raycast를 통해서 충돌을 미리 감지하는 방식.(너무빨리 적이 이동해오면 삑날우려때문에 보정함)
        }
    }

    void OnHitObject(RaycastHit hit) //충돌한 오브젝트를 가져옴
    {
        IDamageable damageableObject = hit.collider.GetComponent<IDamageable>(); //충돌한 오브젝트의 컴포넌트를 get
        if (damageableObject != null) {
            damageableObject.TakeHit(damage, hit);
        }

       // print(hit.collider.gameObject.name);
        GameObject.Destroy(gameObject);
    }

    void OnHitObject(Collider c) //충돌한 오브젝트를 가져옴
    {
        IDamageable damageableObject = c.GetComponent<IDamageable>(); //충돌한 오브젝트의 컴포넌트를 get
        if (damageableObject != null)
        {
            damageableObject.TakeDamage(damage);
        }

        // print(hit.collider.gameObject.name);
        GameObject.Destroy(gameObject);
    }
}
