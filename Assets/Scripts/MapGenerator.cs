﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public Map[] maps;
    public int mapIndex;

    public Transform tilePrefab; //인스턴스화 할 타일.
    public Transform obstaclePrefab;
    public Transform navmeshFloor; //커다란 바닥 매쉬.
                                   /*유니티는 게임 도중에 동적으로 내브메쉬를 굽도록 허락 x 
                               따라서, 커다란 맵을 만들어 거기에 대한 하나의 내브매쉬를 가진 뒤에
                               실제 돌아다닐 맵의 바깥에 위치한 부분(장애물)들을 가려버릴(=마스킹) 겁니다. 
                               보이지않는 바닥. = Navmesh floor */
                                   //obstacle에 추가한 NavMeshObstacle의 carve 옵션을 켜면 동적으로 내브메쉬가 잘라짐.
                                   //carve hole에 대한 부분은 Agent Radius와 관련이 되어있다. AgentRadius를 너무작게하면안됨.(패스를 찾는 시간을 증가시켜버리기때문)
    public Transform navmeshMaskPrefab; //네브매쉬플로어를 마스킹
    //public Vector2 mapSize;
    public Vector2 maxMapSize;

    [Range(0, 1)]
    public float outlinePercent;

    //[Range(0, 1)]
   // public float obstaclePercent; //장애물 갯수 비율

    public float tileSize;

    List<Coord> allTileCoords; //모든 타일 좌표정보
    Queue<Coord> shuffledTileCoords; //셔플된 타일들 

    Map currentMap;

    //public int seed = 10;
    //Coord mapCentre; //맵 중앙은 항상 비워있어야한다.(스폰되는 부분임)

    public void GenerateMap() {
        currentMap = maps[mapIndex];
        System.Random prng = new System.Random(currentMap.seed);//랜덤숫자생성절차.prng
        GetComponent<BoxCollider>().size = new Vector3(currentMap.mapSize.x * tileSize, 0.05f, currentMap.mapSize.y * tileSize);

        //Generating Coords
        allTileCoords = new List<Coord>();
        for (int x = 0; x < currentMap.mapSize.x; x++)
        {
            for (int y = 0; y < currentMap.mapSize.y; y++)
            {
                allTileCoords.Add(new Coord(x, y));
            }
        }
        shuffledTileCoords = new Queue<Coord>(Utility.ShuffleArray(allTileCoords.ToArray(), currentMap.seed));
        //mapCentre = new Coord((int)currentMap.mapSize.x / 2, (int)currentMap.mapSize.y / 2); //맵 정중앙 공간.

        //Create map holder object
        string holderName = "Genereated Map"; //타일들을 자식으로 묶을 오브젝트의 이름.
        if (transform.FindChild(holderName)) {
            DestroyImmediate(transform.FindChild(holderName).gameObject); //DestroyImmediate는 에디터에서 호출용도
        }

        Transform mapHolder = new GameObject(holderName).transform;//타일을 묶을대상.MapGenerator(Map) -> Genereated Map -> tile
        mapHolder.parent = transform;

        //Spawning tiles
        for (int x = 0; x < currentMap.mapSize.x; x++){
            for (int y = 0; y < currentMap.mapSize.y; y++){
                Vector3 tilePosition = CoordToPosition(x, y);
                //-currentMap.mapSize.x/2를 시작점으로 하면 x좌표 0부터 맵의 가로길이절반만큼 왼쪽으로 이동한점부터 타일생성
                Transform newTile = Instantiate(tilePrefab, tilePosition, Quaternion.Euler(Vector3.right * 90)) as Transform;  //Vector3.right는 x축을 나타낸다.
                //바닥에 깔기위해 90도 회전.오일러각(이전좌표축에서 이후좌표축이 얾나회전했는지 기준으로 회전을 표현하는 좌표계)을 기준회전
                newTile.localScale = Vector3.one * (1 - outlinePercent) * tileSize; //outlinePercent가 1이면 전체영역이 테두리(= localScale = 0). 타일크기에 맞춰서 크기조절.       
                newTile.parent = mapHolder;     //새타일의 부모를 mapHolder 로 해줌.
            }
        }

        //Spawning obstacles
        bool[,] obstacleMap = new bool[(int)currentMap.mapSize.x, (int)currentMap.mapSize.y];
        // 새 장애물이 추가되도 맵에 고립된 공간이없는지 플러드필 알고리즘을 통해서 체크. 

        int obstacleCount = (int)(currentMap.mapSize.x * currentMap.mapSize.y * currentMap.obstaclePercent);   //10; 반드시 이 갯수의 장애물이 안생길수있음. 고립된 공간이 되는 위치에 걸릴경우 안만들고 그냥 스킵.
        int currentObstacleCount = 0;
        for (int i = 0; i < obstacleCount; i++)
        {
            Coord randomCoord = GetRandomCoord();
            obstacleMap[randomCoord.x, randomCoord.y] = true;
            currentObstacleCount++;

            if (randomCoord != currentMap.mapCentre && MaplsFullyAccessible(obstacleMap, currentObstacleCount)) {
                float obstacleHeight = Mathf.Lerp(currentMap.minObstacleHeight, currentMap.maxObtacleHeight, (float)prng.NextDouble());
                //Lerp 랜덤퍼센티지 값. 
                Vector3 obstaclePosition = CoordToPosition(randomCoord.x, randomCoord.y);

                Transform newObstacle = Instantiate(obstaclePrefab, obstaclePosition + Vector3.up * obstacleHeight/2, Quaternion.identity) as Transform; //여기선 회전을 안해주냐? 정육면체라서 그런가?
                newObstacle.parent = mapHolder;
                newObstacle.localScale = new Vector3((1-outlinePercent)*tileSize, obstacleHeight, (1-outlinePercent)*tileSize);//Vector3.one * (1 - outlinePercent) * tileSize; //장애물도 타일크기에 맞춰줌.
                //x는 가로 y는 높이 z는 세로

                Renderer objectRenderer = newObstacle.GetComponent<Renderer>();
                Material objectMaterial = new Material(objectRenderer.sharedMaterial);
                float colorPercent = randomCoord.y / (float)currentMap.mapSize.y; //맵의 그라데이션을 위해 현재 오브젝트의 좌표가 얼만큼 앞쪽에 위치한지 표현.
                objectMaterial.color = Color.Lerp(currentMap.foregroundColor, currentMap.backroundColor, colorPercent);

                objectRenderer.sharedMaterial = objectMaterial;
            }
            else //장애물 생성조건에 맞지않은 경우.
            {
                obstacleMap[randomCoord.x, randomCoord.y] = false;
                currentObstacleCount--;
            }
        }

        //Debug.Log("obstacleCount:" + obstacleCount + " currentObstacleCount:" + currentObstacleCount);
        //장애물퍼센트, 시드값이 같을 경우 언제나 같은 맵의 형태가 나온다 왜?. 시드가 같으면 같은 랜덤 값이 나와서그러냐.what

        //Creating navmesh mask
        Transform maskLeft = Instantiate(navmeshMaskPrefab, Vector3.left * (currentMap.mapSize.x + maxMapSize.x) / 4f * tileSize, Quaternion.identity) as Transform; //맵의 왼쪽 가장자리를 마스킹함. 
        maskLeft.parent = mapHolder;
        maskLeft.localScale = new Vector3((maxMapSize.x - currentMap.mapSize.x) / 2f, 1, currentMap.mapSize.y) * tileSize;//최대맵사이즈에서 실제맵가장자리 사이의 거리. 바닥의 가로세로는 x와 z값 기준.

        Transform maskRight = Instantiate(navmeshMaskPrefab, Vector3.right * (currentMap.mapSize.x + maxMapSize.x) / 4f * tileSize, Quaternion.identity) as Transform; //맵의 왼쪽 가장자리를 마스킹함. 
        maskRight.parent = mapHolder;
        maskRight.localScale = new Vector3((maxMapSize.x - currentMap.mapSize.x) / 2f, 1, currentMap.mapSize.y) * tileSize;

        Transform maskTop = Instantiate(navmeshMaskPrefab, Vector3.forward * (currentMap.mapSize.y + maxMapSize.y) / 4f * tileSize, Quaternion.identity) as Transform; //맵의 왼쪽 가장자리를 마스킹함. 
        maskTop.parent = mapHolder;
        maskTop.localScale = new Vector3(maxMapSize.x, 1, (maxMapSize.y - currentMap.mapSize.y) / 2f) * tileSize;

        Transform maskBottom = Instantiate(navmeshMaskPrefab, Vector3.back * (currentMap.mapSize.y + maxMapSize.y) / 4f * tileSize, Quaternion.identity) as Transform; //맵의 왼쪽 가장자리를 마스킹함. 
        maskBottom.parent = mapHolder;
        maskBottom.localScale = new Vector3(maxMapSize.x, 1, (maxMapSize.y - currentMap.mapSize.y) / 2f) * tileSize;

        navmeshFloor.localScale = new Vector3(maxMapSize.x, maxMapSize.y) * tileSize;
        //평면의 크기는 x와 z값을, 높이는 y값을 조정하나. 여기서는 z대신 y값 조정.(x축 중심으로 90도 회전했기때문?)
    }

    //1215
    // Start is called before the first frame update
    void Start()
    {
        GenerateMap();
    }

    bool MaplsFullyAccessible(bool[,] obstacleMap, int currentObstacleCount) { //맵전체가 접근 가능한가. currentObstacleCount 생성된 장애물수
        //맵의 중앙부터 레이더 처럼 타일검색. 장애물이 아닌 타일 숫자 셈.
        //flood fill 알고리즘으로 얻은 값이, 반드시 존재하는 비장애물 타일 갯수와 다르면 알고리즘이 모든타일에 닿지 않았다는 뜻. : return fasle
        //이미 방문한 타일들은 마크해서 같은 타일을 또 보진않게 함.
        bool[,] mapFlags = new bool[obstacleMap.GetLength(0), obstacleMap.GetLength(1)]; //이게 뭐지
        Queue<Coord> queue = new Queue<Coord>(); //what??
        queue.Enqueue(currentMap.mapCentre);
        mapFlags[currentMap.mapCentre.x, currentMap.mapCentre.y] = true;//맵 중앙은 비어있는걸 알기때문에 이미 체크한 것으로 간주

        int accessibleTileCount = 1; //접근 가능한 타일 수.

        while (queue.Count > 0) { //큐안에 좌표가 있는 동안.
            Coord tile = queue.Dequeue();

            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    int neighbourX = tile.x + x; //이웃타일의 좌표. 가운데 타일부터 시작.
                    int neighbourY = tile.y + y;
                    //이웃한 8개의 타일을 순회
                    if (x == 0 || y == 0)  //8개 중 수평수직타일만 보기 위해 대각선 타일은 제외.
                    {
                        if (neighbourX >= 0 && neighbourX < obstacleMap.GetLength(0) //맵배열의 x축의 길이
                            && neighbourY >= 0 && neighbourY < obstacleMap.GetLength(1)) //맵배열의 y축의 길이
                        { //좌표가 obstacleMap 내부인지확인.현재타일이 모서리일때 -1로 가면 bool 배열밖으로 나간다?? what
                            if (!mapFlags[neighbourX, neighbourY] && !obstacleMap[neighbourX, neighbourY]) { //검색하지않은 타일&장애물타일이 아닌 타일.
                                mapFlags[neighbourX, neighbourY] = true;
                                queue.Enqueue(new Coord(neighbourX, neighbourY));
                                accessibleTileCount++;
                            }
                        }
                    }
                }
            }
        }

        int targetAccessibleTileCount = (int)(currentMap.mapSize.x * currentMap.mapSize.y - currentObstacleCount);//비장애물 타일의 본래 기대값.
        return targetAccessibleTileCount == accessibleTileCount;
    }

    Vector3 CoordToPosition(int x, int y) {
        //맵의 가운데는 0,0 기준으로 왼쪽은 마이너스 오른쪽은 플러스.
        return new Vector3(-currentMap.mapSize.x / 2f + 0.5f + x, 0, -currentMap.mapSize.y / 2f + 0.5f + y) * tileSize; //타일 크기에 맞춰 타일간의 간격도 늘림.
        //맵의 가로세로 사이즈에 홀수를 할당시, 네브매쉬가 정확하게 생성이 안되는 문제가 있다.(정수로 나누기를 하기 때문에 소수점이 버려져서 그렇다. 그래서 2f로 나눠야한다.)
    }

    //근데 왜 큐를 써서 첫번째껄 빼고 다시 맨뒤로 돌리고하는건데?? what
    public Coord GetRandomCoord() { //큐에서 다음 아이템을 얻어 랜덤 좌표를 반환.
        Coord randomCoord = shuffledTileCoords.Dequeue(); //셔플된 타일좌표큐의 첫아이템
        shuffledTileCoords.Enqueue(randomCoord);          //랜덤좌표randomCoord를 다시 큐의 마지막으로.
        return randomCoord;
    }

    [System.Serializable]
    public struct Coord { //맵의 모든 타일을 표현한 좌표 Coord
        public int x;
        public int y;

        public Coord(int _x, int _y) { // 생성자.
            x = _x;
            y = _y;
        }

        public static bool operator ==(Coord c1, Coord c2) {
            return c1.x == c2.x && c1.y == c2.y;
        }

        public static bool operator !=(Coord c1, Coord c2)
        {
            return !(c1 == c2);
        }
    }


    [System.Serializable]
    public class Map {
        public Coord mapSize;
        [Range(0,1)]
        public float obstaclePercent;
        public int seed;
        public float minObstacleHeight;
        public float maxObtacleHeight;
        public Color foregroundColor; //전면부 색배치 담당.
        public Color backroundColor; //후반부 색깔

        public Coord mapCentre {
            get{
                return new Coord(mapSize.x / 2, mapSize.y / 2);
            }
        }
    }
}
