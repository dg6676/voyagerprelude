﻿using UnityEngine;

public interface IDamageable
{
    //발사체에 부딪힌 대상이 어떤 스크립트를 달고있는지와 상관없이 맞은 것을 알리고 싶어서 만든 인터페이스
    //발사체와 충돌했다는 것을 통보받아야하는 클래스들은 이 인터페이스를 구현.

    void TakeHit(float damage, RaycastHit hit); //hit 충돌된 지점 등의 정보제공
    void TakeDamage(float damage); //hit 충돌된 지점 등의 정보제공
}
