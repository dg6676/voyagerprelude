﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour //Gun의장비등을 관리 
{
    public Transform weaponHold; //총을 붙잡는 위치
    public Gun startingGun;
    Gun equippedGun; //장착중인 총

    void Start()
    {
        if (startingGun != null) {
            EquipGun(startingGun);
        }
    }

    public void EquipGun(Gun GunToEquip){ //장착할 총
        if (equippedGun != null) { Destroy(equippedGun.gameObject); }
        equippedGun = Instantiate(GunToEquip, weaponHold.position, weaponHold.rotation) as Gun;
        equippedGun.transform.parent = weaponHold; //무기 장착공간에 장착..
    }

    public void Shoot() {
        if (equippedGun != null) {
            equippedGun.Shoot();
        }
    }
}
