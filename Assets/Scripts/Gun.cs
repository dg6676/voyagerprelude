﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public Transform muzzle;            //발사체의 발사위치, 총구 위치
    public Projectile projectile;       //발사체
    public float msBetweenShots = 100;  //연사속도 (발사체 사이의 간격)
    public float muzzleVelocity = 35;   // 총알이 발사되는 순간 총알속력

    float nextShotTime;                 //다음발사시간

    public void Shoot() {

        if (Time.time > nextShotTime) {                         //현재시간이 다음 발사시간 보다 클때만 사격. 연사간격조정.
            nextShotTime = Time.time + msBetweenShots / 1000;   //밀리세컨드를 초로변환. 다음 발사시간을 정해줌.
            Projectile newProjectile = Instantiate(projectile, muzzle.position, muzzle.rotation) as Projectile;
            newProjectile.SetSpeed(muzzleVelocity);
        }
       
    }


}
