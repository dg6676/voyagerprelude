﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingEntity : MonoBehaviour, IDamageable
{
    //체력 등의 처리르 공통으로 처리하기 위해 만든 클래스. 체력이 있는 대상은 상속함.
    public float startingHealth;
    protected float health;
    protected bool dead;

    public event System.Action OnDeath; //delegate(구버전 System.Action) : 다른 메소드의 위치를 가리키고 불러올수있는 타입. C++의 함수포인터
    //이벤트

    protected virtual void Start()
    {
        health = startingHealth;
    }

    public void TakeHit(float damage, RaycastHit hit)
    {
        //Do some stuff here with hit;
        TakeDamage(damage);
    }

    protected void Die() {
        dead = true;
        if (OnDeath != null) {
            OnDeath();
        }
        Destroy(gameObject); //현재 스크립트 붙어있는 오브젝트 제거
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }
}
