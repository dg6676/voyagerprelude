﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (PlayerController))] //pLayer스크립트, player controller 스크립트가 같은오브젝트에 붙어있다는 가정을 위해 강제함.
[RequireComponent(typeof(GunController))]
public class Player : LivingEntity{  //for player input

    public float moveSpped = 5;

    Camera viewCamera;
    PlayerController controller;
    GunController gunController;
    // Use this for initialization

    protected override void Start () {
        base.Start();
        controller = GetComponent<PlayerController>(); //위의 가정에 의해 이부분에서 오류가안난다.
        gunController = GetComponent<GunController>();
        viewCamera = Camera.main;
	}
	
    //카메라로부터 커서의 위치를 통과하도록 ray 쏘아서 바닥에 도착하는 지점이 플레이어가 바라볼 곳이 됩니다.
	// Update is called once per frame
	void Update () {
        //movement input
        Vector3 moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));     // GetAxis를 GetAxisRaw로 변경시, 기본 스무딩 적용 안함.
        Vector3 moveVelocity = moveInput.normalized * moveSpped;    //방향(방향을 가리키는 단위벡터) * 속도
        controller.Move(moveVelocity);

        // look input
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition); //화면상의 마우스 위치를 반환, 해당 위치를 찍고 돌아온 Ray (그런데 무한정 뻗음)
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);//inNormal, inPoint(원점에서의 거리)
        float rayDistance;

        if (groundPlane.Raycast(ray, out rayDistance)) { //out:변수를 참조로 전달. true : 위 ray가 바닥 플레인과 교차한것. 카메라에서 ray가 부딪친 지점까지의 거리를 알수가있다. 
            Vector3 point = ray.GetPoint(rayDistance); //실제 교차지점. 
            Debug.DrawLine(ray.origin, point, Color.red);
            controller.LookAt(point);
        }

        //weapon input
        if (Input.GetMouseButton(0)) {//마우스왼쪽버튼
            gunController.Shoot();
        }
    }
}
