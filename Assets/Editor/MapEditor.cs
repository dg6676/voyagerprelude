﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MapGenerator))]//이 에디터 스크립트가 어떤 클래스나 스크립트를 다루는지 명시.
public class MapEditor : Editor
{
    public override void OnInspectorGUI()
    {
        MapGenerator map = target as MapGenerator; //CustomEditor 키워드로 이 에디터 스크립트가 다룰거라 선언한 오브젝트는 target으로 접근할수있게 자동설정됨.
        if (DrawDefaultInspector()) { //인스펙터에서 값이 바뀌었을떄만 true를 반환.
            map.GenerateMap(); //인스펙터에서 값이 바뀌었을때만 맵을 재생성.
        }

        if (GUILayout.Button("Generate Map")) { //수동으로 맵을 갱신하기 위한 버튼.
            map.GenerateMap();
        }
        
        //base.OnInspectorGUI(); --> 인스펙터가 MapGenerator를 볼때마다 매번 새로 인스턴스화하는거는 성능상 안좋다.
        //MapGenerator 스크립트가 인스펙터 윈도우에 띄어져있는 동안만 동작. 
        //다른 오브젝트가 인스펙터 윈도우에 있으면 동작 x
        Debug.Log("MapGenerator");
    }
}
